import React from "react";

const PrevIcon = (prosp: any) => {
  return (
    <svg
      version="1.0"
      xmlns="http://www.w3.org/2000/svg"
      width="24.000000pt"
      height="24.000000pt"
      viewBox="0 0 24.000000 24.000000"
      preserveAspectRatio="xMidYMid meet"
      {...prosp}
    >
      <g
        transform="translate(0.000000,24.000000) scale(0.100000,-0.100000)"
        fill="#111"
        stroke="none"
      >
        <path
          d="M113 146 l-38 -36 38 -36 c46 -42 63 -37 22 6 l-29 30 29 30 c41 43
   24 48 -22 6z"
        />
      </g>
    </svg>
  );
};

export default PrevIcon;
