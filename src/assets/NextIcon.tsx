import React from "react";

const NextIcon = (props: any) => {
  return (
    <svg
      version="1.0"
      xmlns="http://www.w3.org/2000/svg"
      width="24.000000pt"
      height="24.000000pt"
      viewBox="0 0 24.000000 24.000000"
      preserveAspectRatio="xMidYMid meet"
    >
      <g
        transform="translate(0.000000,24.000000) scale(0.100000,-0.100000)"
        fill="#111"
        stroke="none"
      >
        <path
          d="M80 183 c0 -4 12 -20 27 -35 l27 -28 -29 -30 c-41 -43 -24 -48 22 -7
l38 35 -25 27 c-27 29 -60 50 -60 38z"
        />
      </g>
    </svg>
  );
};

export default NextIcon;
