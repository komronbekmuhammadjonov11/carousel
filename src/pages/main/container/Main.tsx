import React, { Fragment, useEffect, useState } from "react";
import { Carousel, Header } from "../components";
import { IPhoto } from "../types/main";

const Main = () => {
  const MAIN_STYLES = {
    width: "60%",
    height: "90vh",
    padding: "10vh 0",
    margin: "0 auto",
    overflow: "hidden",
  };

  return (
    <Fragment>
      <Header />
      <div style={MAIN_STYLES}>
        <Carousel />
      </div>
    </Fragment>
  );
};

export default Main;
