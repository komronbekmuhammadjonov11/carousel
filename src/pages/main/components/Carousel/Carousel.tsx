import React, { Fragment, useEffect, useState } from "react";
import NextIcon from "../../../../assets/NextIcon";
import PrevIcon from "../../../../assets/PrevIcon";
import {
  CarouselActions,
  CarouselCard,
  NextButton,
  PrevButton,
} from "../../../../components";
import { IPhoto } from "../../types/main";
import "./Carousel.css";

const Carousel = () => {
  const [photos, setPhotos] = useState<IPhoto[]>([]);
  const [activeCardIndex, setActiveCardIndex] = useState<number>(0);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/photos")
      .then((response) => response.json())
      .then((json) => setPhotos(json?.slice(0, 5)));
  }, []);

  const renderClassNameItem = (i: number): string => {
    if (i === activeCardIndex) return "active";
    if (i < activeCardIndex) return "prev-clone";
    if (activeCardIndex < i) return "next-clone";
    return "";
  };

  const nextButtonClick = (): void => {
    if (activeCardIndex < photos.length - 1)
      setActiveCardIndex((prev) => prev + 1);
    else setActiveCardIndex(0);
  };

  const prevButtonClick = (): void => {
    if (activeCardIndex > 0) setActiveCardIndex((prev) => prev - 1);
    else setActiveCardIndex(photos.length - 1);
  };

  return (
    <Fragment>
      <div className="carousel-container">
        <PrevButton onClick={prevButtonClick}>
          <PrevIcon />
        </PrevButton>
        <NextButton onClick={nextButtonClick}>
          <NextIcon />
        </NextButton>
        <CarouselActions
          photos={photos}
          activeCardIndex={activeCardIndex}
          setActiveCardIndex={setActiveCardIndex}
        />
        <ul className="carousel-list">
          {photos?.map((photo: IPhoto, i: number) => {
            return (
              <li
                className={`carousel-list-item ${renderClassNameItem(i)}`}
                key={i}
              >
                <CarouselCard imgUrl={photo?.thumbnailUrl} />
              </li>
            );
          })}
        </ul>
      </div>
    </Fragment>
  );
};

export default Carousel;
