import { ReactNode } from "react";

export interface IPrevButton {
  onClick?: () => void;
  children: ReactNode;
}
