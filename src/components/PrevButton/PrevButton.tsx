import React, { FC } from "react";
import { IPrevButton } from "./PrevButton.types";
import "./PrevButton.css";

const PrevButton: FC<IPrevButton> = ({ children, ...props }) => {
  return (
    <button type="button" className="prev-button" {...props}>
      {children}
    </button>
  );
};

export default PrevButton;
