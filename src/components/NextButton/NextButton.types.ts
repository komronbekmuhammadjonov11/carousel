import { ReactNode } from "react";

export interface INextButton {
  onClick?: () => void;
  children: ReactNode;
}
