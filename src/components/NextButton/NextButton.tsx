import React, { FC } from "react";
import "./NextButton.css";
import { INextButton } from "./NextButton.types";

const NextButton: FC<INextButton> = ({ children, ...props }) => {
  return (
    <button type="button" className="next-button" {...props}>
      {children}
    </button>
  );
};

export default NextButton;
