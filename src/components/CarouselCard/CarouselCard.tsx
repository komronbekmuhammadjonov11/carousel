import React, { FC } from "react";
import { ICarouselCard } from "./CarouselCard.types";
import "./CarouselCard.css";
const CarouselCard: FC<ICarouselCard> = ({ imgUrl }) => {
  return (
    <div className="image-content">
      <img className="image" src={imgUrl} alt="Image" />
    </div>
  );
};

export default CarouselCard;
