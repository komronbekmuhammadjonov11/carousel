import { Dispatch, SetStateAction } from "react";
import { IPhoto } from "../../pages/main/types/main";

export interface ICarouselActions {
  photos: IPhoto[];
  activeCardIndex: number;
  setActiveCardIndex: Dispatch<SetStateAction<number>>;
}
