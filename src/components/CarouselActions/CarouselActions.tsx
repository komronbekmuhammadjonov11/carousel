import React, { FC } from "react";
import { IPhoto } from "../../pages/main/types/main";
import "./CarouselActions.css";
import { ICarouselActions } from "./CarouselActions.types";

const CarouselActions: FC<ICarouselActions> = ({
  activeCardIndex,
  photos,
  setActiveCardIndex,
}) => {
  const renderActionsButtonClassName = (index: number): string => {
    if (activeCardIndex === index) return "active-action-button";
    else return "";
  };

  const actionbuttonClick = (index: number): void => {
    setActiveCardIndex(index);
  };

  return (
    <ul className="carousel-actions">
      {photos?.map((photo: IPhoto, index: number) => (
        <button
          className={`action-button ${renderActionsButtonClassName(index)} `}
          onClick={() => actionbuttonClick(index)}
        />
      ))}
    </ul>
  );
};

export default CarouselActions;
